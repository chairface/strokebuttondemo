//
//  ViewController.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "ViewController.h"

#import "StrokeButton.h"
#import "CircleButton.h"
#import "InfinityButton.h"
#import "SquareButton.h"
#import "TriangleButton.h"
#import "SunButton.h"

@interface ViewController ()
{
    StrokeButton* _cloudButton;
}

- (UIBezierPath*) cloudPath;
- (UIBezierPath*) rainCloudPath;

@end

@implementation ViewController

- (void)viewDidLoad
{
    StrokeButton *newButton;
    
    [super viewDidLoad];

    newButton = [[SquareButton alloc] initWithFrame:self.squareView.bounds];
    [newButton addTarget:self action:@selector(doSquareButton:) forControlEvents:UIControlEventTouchUpInside];
    newButton.highlightColor = [UIColor magentaColor];
    [self.squareView addSubview:newButton];

    // No subclass, no nib, and a toggle.
    newButton = [[StrokeButton alloc] initWithFrame:self.cloudView.bounds];
    newButton.normalPath = self.cloudPath;
    newButton.highlightPath = self.rainCloudPath;
    newButton.highlightWidth = 5.0;
    newButton.shadowOffset = CGSizeZero;
    newButton.strokeColor = [UIColor blackColor];
    [newButton addTarget:self action:@selector(doCloudButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.cloudView addSubview:newButton];
    _cloudButton = newButton;
    
    newButton = [[SunButton alloc] initWithFrame:self.sunView.bounds];
    newButton.shadowOffset = CGSizeMake(0, -0.5);  // Gives a retina hairline shadow. Still visible on non-retina.
    newButton.shadowColor = [UIColor grayColor];
    newButton.normalWidth = 2.0;
    newButton.strokeColor = [UIColor yellowColor];
    newButton.highlightWidth = 2.5;
    [newButton addTarget:self action:@selector(doSunButton:) forControlEvents:UIControlEventTouchUpInside];
    newButton.innerShadow = YES;
    [self.sunView addSubview:newButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction) doInfinityButton:(id)sender
{
    self.label.text= @"Infinity";
}

- (void) doTriangleButton:(id)sender
{
    self.label.text = @"Triangle";
}

- (void) doSquareButton:(id)sender
{
    self.label.text = @"Square";
}

- (IBAction) doCircleButton:(StrokeButton*)sender
{
    self.label.text = @"Circle";
}

- (void) doCloudButton:(id)sender
{
    self.label.text = @"Cloud";
    [_cloudButton setSelected:!_cloudButton.selected];
}

- (void) doSunButton:(id)sender
{
    self.label.text = @"Sun";
    [_cloudButton setSelected:NO];
}

- (UIBezierPath*) cloudPath
{
    //// cloud Drawing
    UIBezierPath* cloudPath = [UIBezierPath bezierPath];
    [cloudPath moveToPoint: CGPointMake(36.42, 13.58)];
    [cloudPath addCurveToPoint: CGPointMake(38.14, 15.83) controlPoint1: CGPointMake(37.11, 14.26) controlPoint2: CGPointMake(37.68, 15.02)];
    [cloudPath addCurveToPoint: CGPointMake(49.84, 19.16) controlPoint1: CGPointMake(42.21, 14.88) controlPoint2: CGPointMake(46.67, 15.99)];
    [cloudPath addCurveToPoint: CGPointMake(53.5, 28.07) controlPoint1: CGPointMake(52.3, 21.62) controlPoint2: CGPointMake(53.52, 24.85)];
    [cloudPath addCurveToPoint: CGPointMake(53.72, 28.28) controlPoint1: CGPointMake(53.57, 28.14) controlPoint2: CGPointMake(53.65, 28.21)];
    [cloudPath addCurveToPoint: CGPointMake(53.72, 41.72) controlPoint1: CGPointMake(57.43, 31.99) controlPoint2: CGPointMake(57.43, 38.01)];
    [cloudPath addCurveToPoint: CGPointMake(40.28, 41.72) controlPoint1: CGPointMake(50.01, 45.43) controlPoint2: CGPointMake(43.99, 45.43)];
    [cloudPath addLineToPoint: CGPointMake(40.11, 41.54)];
    [cloudPath addCurveToPoint: CGPointMake(37.28, 46.28) controlPoint1: CGPointMake(39.59, 43.26) controlPoint2: CGPointMake(38.65, 44.9)];
    [cloudPath addCurveToPoint: CGPointMake(21.72, 46.28) controlPoint1: CGPointMake(32.98, 50.57) controlPoint2: CGPointMake(26.02, 50.57)];
    [cloudPath addCurveToPoint: CGPointMake(20.37, 44.63) controlPoint1: CGPointMake(21.21, 45.76) controlPoint2: CGPointMake(20.76, 45.21)];
    [cloudPath addCurveToPoint: CGPointMake(20.01, 45.01) controlPoint1: CGPointMake(20.25, 44.76) controlPoint2: CGPointMake(20.13, 44.89)];
    [cloudPath addCurveToPoint: CGPointMake(7.99, 45.01) controlPoint1: CGPointMake(16.69, 48.33) controlPoint2: CGPointMake(11.31, 48.33)];
    [cloudPath addCurveToPoint: CGPointMake(7.99, 32.99) controlPoint1: CGPointMake(4.67, 41.69) controlPoint2: CGPointMake(4.67, 36.31)];
    [cloudPath addCurveToPoint: CGPointMake(11.68, 30.82) controlPoint1: CGPointMake(9.05, 31.93) controlPoint2: CGPointMake(10.33, 31.2)];
    [cloudPath addCurveToPoint: CGPointMake(14.28, 22.28) controlPoint1: CGPointMake(11.09, 27.83) controlPoint2: CGPointMake(11.96, 24.6)];
    [cloudPath addCurveToPoint: CGPointMake(18.57, 19.82) controlPoint1: CGPointMake(15.51, 21.05) controlPoint2: CGPointMake(17, 20.23)];
    [cloudPath addCurveToPoint: CGPointMake(21.58, 13.58) controlPoint1: CGPointMake(18.82, 17.54) controlPoint2: CGPointMake(19.83, 15.32)];
    [cloudPath addCurveToPoint: CGPointMake(36.42, 13.58) controlPoint1: CGPointMake(25.68, 9.47) controlPoint2: CGPointMake(32.32, 9.47)];
    [cloudPath closePath];
    return cloudPath;
}

- (UIBezierPath*) rainCloudPath
{
    UIBezierPath *result = [UIBezierPath bezierPath];
    
    //// cloud Drawing
    UIBezierPath* cloudPath = [UIBezierPath bezierPath];
    [cloudPath moveToPoint: CGPointMake(36.42, 13.58)];
    [cloudPath addCurveToPoint: CGPointMake(38.14, 15.83) controlPoint1: CGPointMake(37.11, 14.26) controlPoint2: CGPointMake(37.68, 15.02)];
    [cloudPath addCurveToPoint: CGPointMake(49.84, 19.16) controlPoint1: CGPointMake(42.21, 14.88) controlPoint2: CGPointMake(46.67, 15.99)];
    [cloudPath addCurveToPoint: CGPointMake(53.5, 28.07) controlPoint1: CGPointMake(52.3, 21.62) controlPoint2: CGPointMake(53.52, 24.85)];
    [cloudPath addCurveToPoint: CGPointMake(53.72, 28.28) controlPoint1: CGPointMake(53.57, 28.14) controlPoint2: CGPointMake(53.65, 28.21)];
    [cloudPath addCurveToPoint: CGPointMake(53.72, 41.72) controlPoint1: CGPointMake(57.43, 31.99) controlPoint2: CGPointMake(57.43, 38.01)];
    [cloudPath addCurveToPoint: CGPointMake(50.72, 43.74) controlPoint1: CGPointMake(52.83, 42.61) controlPoint2: CGPointMake(51.81, 43.28)];
    [cloudPath addLineToPoint: CGPointMake(47.5, 36.5)];
    [cloudPath addLineToPoint: CGPointMake(44.14, 44.06)];
    [cloudPath addCurveToPoint: CGPointMake(40.28, 41.72) controlPoint1: CGPointMake(42.73, 43.62) controlPoint2: CGPointMake(41.4, 42.84)];
    [cloudPath addLineToPoint: CGPointMake(40.11, 41.54)];
    [cloudPath addCurveToPoint: CGPointMake(37.28, 46.28) controlPoint1: CGPointMake(39.59, 43.26) controlPoint2: CGPointMake(38.65, 44.9)];
    [cloudPath addCurveToPoint: CGPointMake(21.72, 46.28) controlPoint1: CGPointMake(32.98, 50.57) controlPoint2: CGPointMake(26.02, 50.57)];
    [cloudPath addCurveToPoint: CGPointMake(20.37, 44.63) controlPoint1: CGPointMake(21.21, 45.76) controlPoint2: CGPointMake(20.76, 45.21)];
    [cloudPath addCurveToPoint: CGPointMake(20.01, 45.01) controlPoint1: CGPointMake(20.25, 44.76) controlPoint2: CGPointMake(20.13, 44.89)];
    [cloudPath addCurveToPoint: CGPointMake(7.99, 45.01) controlPoint1: CGPointMake(16.69, 48.33) controlPoint2: CGPointMake(11.31, 48.33)];
    [cloudPath addCurveToPoint: CGPointMake(7.99, 32.99) controlPoint1: CGPointMake(4.67, 41.69) controlPoint2: CGPointMake(4.67, 36.31)];
    [cloudPath addCurveToPoint: CGPointMake(11.68, 30.82) controlPoint1: CGPointMake(9.05, 31.93) controlPoint2: CGPointMake(10.33, 31.2)];
    [cloudPath addCurveToPoint: CGPointMake(14.28, 22.28) controlPoint1: CGPointMake(11.09, 27.83) controlPoint2: CGPointMake(11.96, 24.6)];
    [cloudPath addCurveToPoint: CGPointMake(18.57, 19.82) controlPoint1: CGPointMake(15.51, 21.05) controlPoint2: CGPointMake(17, 20.23)];
    [cloudPath addCurveToPoint: CGPointMake(21.58, 13.58) controlPoint1: CGPointMake(18.82, 17.54) controlPoint2: CGPointMake(19.83, 15.32)];
    [cloudPath addCurveToPoint: CGPointMake(36.42, 13.58) controlPoint1: CGPointMake(25.68, 9.47) controlPoint2: CGPointMake(32.32, 9.47)];
    [cloudPath closePath];
    [result appendPath:cloudPath];

    //// drop Drawing
    UIBezierPath* dropPath = [UIBezierPath bezierPath];
    [dropPath moveToPoint: CGPointMake(47.5, 36.5)];
    [dropPath addLineToPoint: CGPointMake(43.5, 45.5)];
    [dropPath addCurveToPoint: CGPointMake(47.5, 49.5) controlPoint1: CGPointMake(43.5, 45.5) controlPoint2: CGPointMake(43.9, 49.5)];
    [dropPath addCurveToPoint: CGPointMake(51.5, 45.5) controlPoint1: CGPointMake(51.1, 49.5) controlPoint2: CGPointMake(51.5, 45.5)];
    [dropPath addLineToPoint: CGPointMake(47.5, 36.5)];
    [dropPath closePath];
    [result appendPath:dropPath];

    return result;    
}



@end
