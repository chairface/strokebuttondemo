//
//  StrokeButton.h
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/27/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrokeButton : UIButton

+ (void) setDefaultMargin:(CGSize)margin;
+ (void) setDefaultStrokeColor:(UIColor*)strokeColor;
+ (void) setDefaultHighlightColor:(UIColor *)highlightColor;
+ (void) setDefaultShadowColor:(UIColor *)shadowColor;
+ (void) setDefaultDisabledColor:(UIColor *)disabledColor;
+ (void) setDefaultShadowOffset:(CGSize)shadowOffset;
+ (void) setDefaultNormalWidth:(CGFloat)normalWidth;
+ (void) setDefaultHighlihghtWidth:(CGFloat)highlightWidth;
+ (void) setDefaultInnerShadow:(BOOL)innerShadow;

@property (nonatomic, strong) UIBezierPath *normalPath;
@property (nonatomic, strong) UIBezierPath *highlightPath;

@property (nonatomic) CGSize margin UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *strokeColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *shadowColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *highlightColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *disabledColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGSize shadowOffset UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat normalWidth UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat highlightWidth UI_APPEARANCE_SELECTOR;
@property (nonatomic) BOOL innerShadow UI_APPEARANCE_SELECTOR;
@property (nonatomic) BOOL recenter UI_APPEARANCE_SELECTOR;

@end
