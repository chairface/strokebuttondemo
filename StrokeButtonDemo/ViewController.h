//
//  ViewController.h
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *label;

// @property (nonatomic, strong) IBOutlet UIView *circleView;
@property (nonatomic, strong) IBOutlet UIView *squareView;
@property (nonatomic, strong) IBOutlet UIView *triangleView;
//@property (nonatomic, strong) IBOutlet UIView *infiniteView;

@property (nonatomic, strong) IBOutlet UIView *cloudView;  // No subclass test.
@property (nonatomic, strong) IBOutlet UIView *sunView;

- (IBAction) doCircleButton:(id)sender;
- (IBAction) doInfinityButton:(id)sender;

@end
