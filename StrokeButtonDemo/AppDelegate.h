//
//  AppDelegate.h
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
