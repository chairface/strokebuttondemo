//
//  StrokeButton.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/27/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "StrokeButton.h"

// "d" prefix stands for default.
static CGSize   dMargin = (CGSize){2.0, 2.0};
static UIColor  *dStrokeColor;
static UIColor  *dHighlightColor;
static UIColor  *dShadowColor;
static UIColor  *dDisabledColor;
static CGSize   dShadowOffset = (CGSize){0, -1};
static CGFloat  dNormalWidth = 1.0;
static CGFloat  dHighlightWidth = 2.0;
static BOOL     dInnerShadow = NO;
static BOOL     dRecenter = YES;

@interface StrokeButton ()

- (void) initCommon;
- (UIBezierPath*) centerPath:(UIBezierPath*)path;

@end

@implementation StrokeButton

#pragma mark - Default setters

+ (void) setDefaultMargin:(CGSize)margin
{
    dMargin = margin;
}

+ (void) setDefaultStrokeColor:(UIColor*)strokeColor
{
    dStrokeColor = strokeColor;
}

+ (void) setDefaultHighlightColor:(UIColor *)highlightColor
{
    dHighlightColor = highlightColor;
}

+ (void) setDefaultShadowColor:(UIColor *)shadowColor
{
    dShadowColor = shadowColor;
}

+ (void) setDefaultDisabledColor:(UIColor *)disabledColor
{
    dDisabledColor = disabledColor;
}

+ (void) setDefaultShadowOffset:(CGSize)shadowOffset
{
    dShadowOffset = shadowOffset;
}

+ (void) setDefaultNormalWidth:(CGFloat)normalWidth
{
    dNormalWidth = normalWidth;
}

+ (void) setDefaultHighlihghtWidth:(CGFloat)highlightWidth
{
    dHighlightWidth = highlightWidth;
}

+ (void) setDefaultInnerShadow:(BOOL)innerShadow
{
    dInnerShadow = innerShadow;
}

+ (void) setDefaultRecenter:(BOOL)recenter
{
    dRecenter = recenter;
}

#pragma mark - Instance behaviors

- (void) initCommon
{
    [self setTitle:@"" forState:UIControlStateNormal];
    [self setTitle:@"" forState:UIControlStateHighlighted];
    [self setTitle:@"" forState:UIControlStateDisabled];
    [self setTitle:@"" forState:UIControlStateSelected];

    if (self.buttonType != UIButtonTypeCustom) {
        NSLog(@"StrokeButtons must be set to UIButtonTypeCustom");
    }

    _margin = dMargin;
    _strokeColor = dStrokeColor ? dStrokeColor : [UIColor colorWithRed:0.0 green:0.49 blue:1.0 alpha:1.0];
    _shadowColor = dShadowColor ? dShadowColor : [UIColor blackColor];
    _highlightColor = dHighlightColor ? dHighlightColor : [UIColor greenColor];
    _disabledColor = dDisabledColor ? dDisabledColor : [UIColor grayColor];
    _shadowOffset = dShadowOffset;
    _normalWidth = dNormalWidth;
    _highlightWidth = dHighlightWidth;
    _innerShadow = dInnerShadow;
    _recenter = dRecenter;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initCommon];
    }
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    [self initCommon];
}

- (UIBezierPath*) centerPath:(UIBezierPath*)inPath
{
    if (self.recenter) {
        UIBezierPath *path = [inPath copy];
        CGRect pathBounds = [path bounds];
        CGRect insetBounds = CGRectInset(self.bounds, self.margin.width, self.margin.height);

        // Div 0 guard
        if (pathBounds.size.width == 0.0 || pathBounds.size.height == 0.0 ||
            self.bounds.size.width == 0.0 || self.bounds.size.height == 0.0) {
            return nil;
        }

        CGAffineTransform scale;
        CGFloat srcRatio = pathBounds.size.height/pathBounds.size.width;
        CGFloat destRatio = self.bounds.size.height/self.bounds.size.width;

        if (srcRatio<destRatio) {
            scale = CGAffineTransformMakeScale(insetBounds.size.width/pathBounds.size.width,
                                               insetBounds.size.width/pathBounds.size.width);
        }
        else {
            scale = CGAffineTransformMakeScale(insetBounds.size.height/pathBounds.size.height,
                                               insetBounds.size.height/pathBounds.size.height);
        }

        [path applyTransform:scale];

        pathBounds = [path bounds];
        CGPoint pathCenter = CGPointMake(CGRectGetMidX(pathBounds),CGRectGetMidY(pathBounds));
        CGPoint myCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));

        CGAffineTransform translate = CGAffineTransformMakeTranslation(-pathCenter.x + myCenter.x,
                                                                       -pathCenter.y + myCenter.y);
        [path applyTransform:translate];
        return path;
    }
    else {
        return inPath;
    }
}

/**
 * By default the highlight path is just the normal path.
 */
- (UIBezierPath*) highlightPath
{
    if (_highlightPath) {
        return _highlightPath;
    }
    return self.normalPath;
}


- (void) normalImage:(CGContextRef)ctxt
{
    UIBezierPath *path = self.normalPath;
    path = [self centerPath:path];

    if (self.innerShadow) {
        // Set up the path as the clip region. Fill with strokecolor
        // Draw the shadow path at shadowoffset in shadow color
        // Erase part of the shadow by drawing with strokecolor at shadowoffset.
        if (self.shadowColor == nil || CGSizeEqualToSize(self.shadowOffset, CGSizeZero)) {
            NSLog(@"StrokeButton error: Drawing an inner shadow without a correct shadow setup.");
        }
        else {
            // First set our clip to be the stroke of the button.
            CGContextSaveGState(ctxt);
            CGContextBeginPath(ctxt);
            CGContextAddPath(ctxt, path.CGPath);
            CGContextSetLineWidth(ctxt, self.normalWidth);
            CGContextReplacePathWithStrokedPath(ctxt);
            CGContextClip(ctxt);

            // Fill the clip with the shadow.
            CGContextSetFillColorWithColor(ctxt, self.shadowColor.CGColor);
            CGContextFillRect(ctxt, CGContextGetClipBoundingBox(ctxt));

            // Draw unshadow
            CGContextSetStrokeColorWithColor(ctxt, self.strokeColor.CGColor);
            CGContextTranslateCTM(ctxt, -self.shadowOffset.width, -self.shadowOffset.height);
            CGContextAddPath(ctxt, path.CGPath);
            CGContextSetLineWidth(ctxt, self.normalWidth);
            CGContextStrokePath(ctxt);

            // Finally, restore state.
            CGContextRestoreGState(ctxt);
        }
    }
    else {
        // Shadow goes first
        path.lineWidth = self.normalWidth;

        if (self.shadowColor && !CGSizeEqualToSize(self.shadowOffset, CGSizeZero)) {
            CGContextSaveGState(ctxt);
            CGContextTranslateCTM(ctxt, -self.shadowOffset.width, -self.shadowOffset.height);

            [self.shadowColor setStroke];
            [path stroke];
            CGContextRestoreGState(ctxt);
        }

        [self.strokeColor setStroke];
        [path stroke];
    }
}

- (void) highlightImage:(CGContextRef)ctxt
{
    UIBezierPath *path = self.highlightPath;
    path = [self centerPath:path];

    // Shadow goes first
    path.lineWidth = self.normalWidth;

    if (self.shadowColor && !CGSizeEqualToSize(self.shadowOffset, CGSizeZero)) {
        CGContextSaveGState(ctxt);
        CGContextTranslateCTM(ctxt, -self.shadowOffset.width, -self.shadowOffset.height);

        [self.shadowColor setStroke];
        [path stroke];
        CGContextRestoreGState(ctxt);
    }

    CGContextSaveGState(ctxt);
    CGSize flareOffset = CGSizeMake(0.0, 0.0);
    CGFloat flareRadius = 5;    // XXX Maybe this should be adjustable
    path.lineWidth = self.highlightWidth;

    CGContextSetShadowWithColor(ctxt, flareOffset, flareRadius, self.highlightColor.CGColor);
    [self.highlightColor setStroke];
    [path stroke];
    CGContextRestoreGState(ctxt);
}

- (void) disabledImage:(CGContextRef)ctxt
{
    // I haven't really settled on a disabled style yet.
    UIBezierPath *path = self.normalPath;
    path = [self centerPath:path];
    CGFloat dashes[] = {1.5, 1.5};

    CGContextSetLineDash(ctxt, 0, dashes, 2);
    [self.disabledColor setStroke];
    [path stroke];
}

- (void) selectedImage:(CGContextRef)ctxt
{
    [self highlightImage:ctxt]; // I guess this will do for now.
}

- (void) drawRect:(CGRect)rect
{
    CGContextRef ctxt = UIGraphicsGetCurrentContext();

    if (self.enabled) {
        if (self.highlighted || self.selected) {
            // Highlighted or selected. Same for now.
            [self selectedImage:ctxt];
        }
        else {
            // Normal
            [self normalImage:ctxt];
        }
    }
    else {
        // Disabled
        [self disabledImage:ctxt];
    }
}

// This forces the redraw at the righ times. I think.
- (void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

- (void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self setNeedsDisplay];
}

@end
