//
//  CircleButton.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "CircleButton.h"

@implementation CircleButton

- (UIBezierPath*) normalPath
{
    //// Oval Drawing
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(1.5, 1.5, 57, 57)];
    return ovalPath;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.highlightColor = [UIColor yellowColor];
    self.shadowColor = nil;
}

@end
