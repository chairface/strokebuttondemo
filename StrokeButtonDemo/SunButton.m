//
//  SunButton.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/21/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "SunButton.h"

@implementation SunButton

- (UIBezierPath*) normalPath
{
    UIBezierPath *result = [UIBezierPath bezierPath];
    
    UIBezierPath* pointsPath = [UIBezierPath bezierPath];
    [pointsPath moveToPoint: CGPointMake(34.29, 13.53)];
    [pointsPath addCurveToPoint: CGPointMake(25.71, 13.53) controlPoint1: CGPointMake(31.47, 12.86) controlPoint2: CGPointMake(28.53, 12.86)];
    [pointsPath addLineToPoint: CGPointMake(30, 1.71)];
    [pointsPath addLineToPoint: CGPointMake(34.29, 13.53)];
    [pointsPath closePath];
    [pointsPath moveToPoint: CGPointMake(18.69, 16.91)];
    [pointsPath addCurveToPoint: CGPointMake(17, 18.41) controlPoint1: CGPointMake(18.1, 17.37) controlPoint2: CGPointMake(17.54, 17.87)];
    [pointsPath addCurveToPoint: CGPointMake(13.34, 23.61) controlPoint1: CGPointMake(15.44, 19.97) controlPoint2: CGPointMake(14.22, 21.73)];
    [pointsPath addLineToPoint: CGPointMake(6.77, 12.89)];
    [pointsPath addLineToPoint: CGPointMake(18.69, 16.91)];
    [pointsPath closePath];
    [pointsPath moveToPoint: CGPointMake(46.66, 23.61)];
    [pointsPath addCurveToPoint: CGPointMake(43, 18.41) controlPoint1: CGPointMake(45.78, 21.73) controlPoint2: CGPointMake(44.56, 19.97)];
    [pointsPath addCurveToPoint: CGPointMake(41.31, 16.91) controlPoint1: CGPointMake(42.46, 17.87) controlPoint2: CGPointMake(41.9, 17.37)];
    [pointsPath addLineToPoint: CGPointMake(53.23, 12.89)];
    [pointsPath addLineToPoint: CGPointMake(46.66, 23.61)];
    [pointsPath closePath];
    [pointsPath moveToPoint: CGPointMake(58.96, 38.03)];
    [pointsPath addLineToPoint: CGPointMake(46.49, 39.57)];
    [pointsPath addCurveToPoint: CGPointMake(48.39, 31.21) controlPoint1: CGPointMake(47.79, 36.95) controlPoint2: CGPointMake(48.42, 34.08)];
    [pointsPath addLineToPoint: CGPointMake(58.96, 38.03)];
    [pointsPath closePath];
    [pointsPath moveToPoint: CGPointMake(1.04, 38.03)];
    [pointsPath addLineToPoint: CGPointMake(11.61, 31.21)];
    [pointsPath addCurveToPoint: CGPointMake(13.51, 39.57) controlPoint1: CGPointMake(11.58, 34.08) controlPoint2: CGPointMake(12.21, 36.95)];
    [pointsPath addLineToPoint: CGPointMake(1.04, 38.03)];
    [pointsPath closePath];
    [pointsPath moveToPoint: CGPointMake(42.89, 58.18)];
    [pointsPath addLineToPoint: CGPointMake(33.9, 49.39)];
    [pointsPath addCurveToPoint: CGPointMake(41.62, 45.67) controlPoint1: CGPointMake(36.66, 48.79) controlPoint2: CGPointMake(39.32, 47.55)];
    [pointsPath addLineToPoint: CGPointMake(42.89, 58.18)];
    [pointsPath closePath];
    [pointsPath moveToPoint: CGPointMake(18.39, 45.68)];
    [pointsPath addCurveToPoint: CGPointMake(26.1, 49.39) controlPoint1: CGPointMake(20.68, 47.55) controlPoint2: CGPointMake(23.34, 48.79)];
    [pointsPath addLineToPoint: CGPointMake(17.11, 58.18)];
    [pointsPath addLineToPoint: CGPointMake(18.38, 45.67)];
    [pointsPath addLineToPoint: CGPointMake(18.39, 45.68)];
    [pointsPath closePath];

    [result appendPath:pointsPath];

    //// heart Drawing
    UIBezierPath* heartPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(15.5, 17.5, 28, 28)];
    [result appendPath:heartPath];

    return result;
}

@end
