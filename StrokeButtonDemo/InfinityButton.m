//
//  InfinityButton.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "InfinityButton.h"

@implementation InfinityButton

- (UIBezierPath*) normalPath
{
    //// infinity Drawing
    UIBezierPath* infinityPath = [UIBezierPath bezierPath];
    [infinityPath moveToPoint: CGPointMake(30, 30)];
    [infinityPath addLineToPoint: CGPointMake(15.75, 15.5)];
    [infinityPath addCurveToPoint: CGPointMake(1.5, 30) controlPoint1: CGPointMake(15.75, 15.5) controlPoint2: CGPointMake(1.5, 15.85)];
    [infinityPath addCurveToPoint: CGPointMake(15.75, 44.5) controlPoint1: CGPointMake(1.5, 44.15) controlPoint2: CGPointMake(15.75, 44.5)];
    [infinityPath addLineToPoint: CGPointMake(44.25, 15.5)];
    [infinityPath addCurveToPoint: CGPointMake(58.5, 30) controlPoint1: CGPointMake(44.25, 15.5) controlPoint2: CGPointMake(58.5, 15.5)];
    [infinityPath addCurveToPoint: CGPointMake(44.25, 44.5) controlPoint1: CGPointMake(58.5, 44.5) controlPoint2: CGPointMake(44.25, 44.5)];
    [infinityPath addLineToPoint: CGPointMake(30, 30)];
    [infinityPath closePath];
    return infinityPath;
}

- (UIColor*) strokeColor
{
    return [UIColor redColor];
}

- (CGSize) shadowOffset
{
    return CGSizeMake(1.0, -3.0);
}

- (void) awakeFromNib
{
    [super awakeFromNib];
}

@end
