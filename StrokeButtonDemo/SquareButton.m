//
//  SquareButton.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "SquareButton.h"

@implementation SquareButton

- (UIBezierPath*) normalPath
{
    //// square Drawing
    UIBezierPath* squarePath = [UIBezierPath bezierPath];
    [squarePath moveToPoint: CGPointMake(13.12, 53.59)];
    [squarePath addLineToPoint: CGPointMake(55.59, 46.88)];
    [squarePath addLineToPoint: CGPointMake(48.88, 4.41)];
    [squarePath addLineToPoint: CGPointMake(6.41, 11.12)];
    [squarePath addLineToPoint: CGPointMake(13.12, 53.59)];
    [squarePath closePath];
    return squarePath;
}

@end
