//
//  TriangleButton.m
//  StrokeButtonDemo
//
//  Created by Charlie Reiman on 6/17/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "TriangleButton.h"

@implementation TriangleButton

- (UIBezierPath*) normalPath
{
    //// triangle Drawing
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint: CGPointMake(5.61, 58.15)];
    [trianglePath addLineToPoint: CGPointMake(43.39, 1.85)];
    [trianglePath addLineToPoint: CGPointMake(56.69, 40.63)];
    [trianglePath addLineToPoint: CGPointMake(5.61, 58.15)];
    [trianglePath closePath];
    return trianglePath;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.shadowColor = [UIColor whiteColor];
    self.enabled = NO;
}

@end
